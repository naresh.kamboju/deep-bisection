# Deep bisection

This performs normal kernel bisections, except it build-tests 7 kernels
in one single step thanks to [Tuxbuild](https://tuxsuite.com/). That is
3-levels depth in a binary bisection.

It requires a local Git repo of the kernel, the revision presenting the
old behavior (passing), and the Tuxbuild reproducer.

## Example deep bisection

SuperH fails to build on `next-20210129`:
https://gitlab.com/Linaro/lkft/mirrors/next/linux-next/-/pipelines/248557601

The failures look like this:
https://gitlab.com/Linaro/lkft/mirrors/next/linux-next/-/jobs/994639152

Exempli gratia, one of the failures:
```
Fail (2 errors) with status message 'failure while building tuxmake target(s):
kernel debugkernel': b01f250d83f6 ("Add linux-next specific files for 20210129")
sh (allnoconfig) with gcc-9 @
https://builds.tuxbuild.com/1njiyI3hC1KuHxqJRlQ9ywXugvm/
```

A Git driven bisection goes like this:
```
git bisect start
git bisect old linux-next/stable
git bisect new next-20210129
```

The next kernel to build would be `7b7f6e418b71`. If that builds correctly, then
the next one to try is `4a8b64f4e85b`. If the first one failed, however, then
the next one to try is `9d51f4608e6d`.

If both `7b7f6e418b71` and `4a8b64f4e85b` passed, then the next kernel to build
is `79a914d7f707`, but if the second failed, then we need to build-test
`a6b8720c2f85`

Now, on the other hand, if `7b7f6e418b71` failed and `9d51f4608e6d` passed, then
we should be looking at `9b046b31ea90`. But if `9d51f4608e6d` failed instead,
the next one to test is `508b0214914b`.

Enter Tuxbuild.

Instead of building one kernel at a time, Tuxbuild allows us to build a bunch of
them very quickly and at the same time.

In this case, we would build all options, so by the time we're done with the
first kernel (`7b7f6e418b71`) we have also built the next candidates
(`4a8b64f4e85b`, `9d51f4608e6d`, `79a914d7f707`, `a6b8720c2f85`, `9b046b31ea90`,
and `508b0214914b`), so we jump in the bisection from step 1 straight into step
4.


### Procedure

Get the kernel tree:
```
git clone \
  -o linux-next \
  https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git \
  /data/linux-next
```

Bisect from `linux-next/stable` (old behavior, building fine) until
`next-20210129` (new behavior, not building):
```
./deep-bisection.sh \
  -C /data/linux-next \
  -o linux-next/stable \
  -t https://builds.tuxbuild.com/1njiyI3hC1KuHxqJRlQ9ywXugvm/
```

## Another example

Arm 32-bits fails to build:
https://gitlab.com/Linaro/lkft/mirrors/torvalds/linux-mainline/-/pipelines/234310399

Error:
```
Fail (9 errors) with status message 'failure while building tuxmake target(s):
kernel debugkernel': 3913d00ac51a ("Merge tag 'irq-core-2020-12-23' of
git://git.kernel.org/pub/scm/linux/kernel/git/tip/tip") arm (defconfig) with
gcc-9 @ https://builds.tuxbuild.com/1m7cXpGl3LBhh47uPAaLWLixcNC/
```

Previous pipeline (for `58cf05f597b0`) builds fine.

Bisect from `v5.10-12963-g58cf05f597b0` (old behavior, building fine) until
`v5.10-13171-g3913d00ac51a` (new behavior, not building):
```
./deep-bisection.sh \
  -C /data/linux-mainline \
  -o v5.10-12963-g58cf05f597b0 \
  -t https://builds.tuxbuild.com/1m7cXpGl3LBhh47uPAaLWLixcNC/
```
