#!/bin/bash

COMMITS_FILE="${1:-commits.txt}"

root_dir="$(dirname "$(readlink -e "$0")")"

"${root_dir}/c2dot.sh" "${COMMITS_FILE}" | dot -Tpng > "${COMMITS_FILE}.png"
